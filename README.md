# print_logo

Imprime en consola una *imagen ASCII* centrada horizontalmente en la consola. Probado en MacOS, Linux, RaspberryOS y [Termux](https://github.com/termux). N.B.: En Windows **NO** funciona.

<img src="./screenshot.png" alt="Screenshot" style="width:80%;"/>

## Requerimientos

`awk`

## Uso

`python3 /ruta/print_logo/print.py </path/to/ascii_image.txt>`  
