import os
import sys

def main ():
	str_archivo = sys.argv [1]
	terminal_size = os.get_terminal_size ()

	arr_lineas = []
	file = open ( str_archivo, "r" )
	arr_lineas = file.readlines ()
	file.close()  

	cmd_ancho_linea = 'cat ' + str_archivo + ' | awk \'{print length, $0}\' | sort -nr | head -1 | cut -d \' \' -f 1';
	stream = os.popen ( cmd_ancho_linea )
	output = stream.read()
	output

	espacios = ( ( terminal_size.columns ) -  int ( output.strip () ) ) // 2 * " "

	str_salida = "";

	for a in arr_lineas :
	    str_salida += espacios + a

	print ( "\n\n", str_salida )

if __name__ == "__main__" :
	main()